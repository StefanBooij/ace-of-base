const pokemon = require('../config/mock-data/pokemon');

const pokemonRepository = {
    async getAll() {
        return pokemon;
    }
};

module.exports = pokemonRepository;
