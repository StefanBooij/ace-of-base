const bestiary = require('express').Router();
const PokemonService = require('../services/pokemonService');

const pokemonService = new PokemonService();

bestiary.get('/', async (req, res) => {
    res.render('bestiary', {
        pokemon: await pokemonService.getAll()
    });
});

module.exports = bestiary;
