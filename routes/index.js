module.exports = (app) => {
    const router = require('express').Router();

    router.use('/', require('./home'));
    router.use('/bestiary', require('./bestiary'));

    return router;
};
