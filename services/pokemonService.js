class PokemonService {
    constructor() {
        this.repository = require('../repositories/pokemonRepository')
    }

    async getAll() {
        return await this.repository.getAll();
    }
}

module.exports = PokemonService;
