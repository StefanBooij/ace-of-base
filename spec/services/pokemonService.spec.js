const PokemonService = require('../../services/pokemonService');

describe('PokemonService', () => {
    let service;

    beforeEach(() => {
        service = new PokemonService();
    });

    afterEach(() => {
        service = null;
    });

    it('Should return an array of pokemon', async () => {
        const pokemon = await service.getAll();

        expect(pokemon.length > 0).toBe(true);
    });

    describe('Nested test', () => {
        it('Should contain a Ghost type', async () => {
            const all = await service.getAll();

            const ghostTypes = all
                .filter(pkmn => {
                    for (const badge of pkmn.badges) {
                        if (badge.name.toLowerCase() === 'ghost') {
                            return true;
                        }
                    }

                    return false;
                });

            expect(ghostTypes.length > 0).toBe(true);
        })
    })
});

